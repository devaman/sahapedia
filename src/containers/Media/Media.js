import React from 'react';
import M1 from '../../assets/m2.jpg'
import './Media.css';
import Navbar from '../../components/Navbar/Navbar'
import Banner from '../../components/Media/Banner/Banner';
import Images from '../../components/Media/Images/Images';
import ImagesCarousel from '../../components/Media/Images/ImagesCarousel';
import Videos from '../../components/Media/Videos/Videos';
import Articles from '../../components/Media/Articles/Articles';
import Tiles from '../../components/Tiles/Tiles';
import { faArrowLeft } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Image1 from '../../assets/s1.jpg'
import Image2 from '../../assets/s2.jpg'
import Image3 from '../../assets/s3.jpg'
import Image4 from '../../assets/s4.jpg'
import Image7 from '../../assets/s7.jpg'
import Image8 from '../../assets/s8.jpg'
import { Link, withRouter } from 'react-router-dom';
const data = [
    {
      heading: "Adishakti: An avant-garde language of theatre",
      image: Image1,
      headColor: "white",
  
      description: "Nothing to show"
    }, {
      heading: "Descendants of Prajapati: The Kumhar Community of Potters",
      image: Image2,
      headColor: "white",
      link:"/media",
      description: "Nothing to show"
  
    },
     {
      heading: "The Art of Aromas: Ittar Industry of Kannauj",
      image: Image7,
      description: "Nothing to show",
      headColor: "white",
  
    },
    {
      heading: "Taleems of Kolhapur: A Legacy in Wrestling",
      image: Image8,
      headColor: "white",
  
      description: "Nothing to show"
    }
  ]
const Media = (props)=>{
    return(<>
            <Navbar type={2}/>
        <div className="media-page">
            <Link to='/'><FontAwesomeIcon icon={faArrowLeft} style={{color:"#fff",margin:"20px",marginLeft:'none',fontSize:"20px"}}/></Link>
            <Banner image={M1} text=""/>
            <div className="flex" style={{margin:"20px 10px"}}>
                {/* <div style={{width: "50%", height:"450px",overflow:"hidden"}} id="Gallery">
                  <Images/>
                </div> */}

                <ImagesCarousel style={{zIndex:1000}}/>
                
                <Videos/>
                <a href="#" style={{color:"#3f6",border:"1px solid #3f6",textDecoration:"none",fontSize:"20px",verticalAlign:"center",height:"max-content",boxSizing:"border-box",marginLeft:"10px", padding:"10px 20px"}}>View All</a>
            </div>
            <Articles/>
            <Tiles type={2} history = {props.history} data={data}/>
        </div>
        </>
    )
}
export default withRouter(Media);