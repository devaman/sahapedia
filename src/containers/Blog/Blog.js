import React, { Component } from 'react';
import Navbar from '../../components/Navbar/Navbar';
import Banner from '../../components/Banner/Banner';
import BannerImage from '../../assets/banner.jpeg'
import Fig1 from '../../assets/fig1.jpeg'
import Fig2 from '../../assets/fig2.jpeg'
import Fig3 from '../../assets/adi_3.jpeg'
import Fig4 from '../../assets/banner.jpeg'
import SectionMap from '../../components/SectionMap/SectionMap';
import Author from '../../components/Author/Author';
import Title from '../../components/Title/Title';
import Content from '../../components/Content/Content';
import EmbedImage from '../../components/EmbedImage/EmbedImage';
import Heading from '../../components/Heading/Heading';
import Tooltip from 'react-png-tooltip'
import './Blog.css'
import FullSizeImage from '../../components/FullSizeImage/FullSizeImage';
import Tags from '../../components/Tags/Tags';
import Bibliography from '../../components/Bibliography/Bibliography';
import Footer from '../../components/Footer/Footer';
import YtWrap from 'react-yt-wrap';
import CarouselSlider from 'react-carousel-slider';
import {
    Accordion,
    AccordionItem,
    AccordionItemHeading,
    AccordionItemButton,
    AccordionItemPanel,
} from 'react-accessible-accordion';
import ImageGallery from "../../components/ImageGallery/ImageGallery";
import 'react-accessible-accordion/dist/fancy-example.css';

class Blog extends Component {
    state = {
        time:0
    }
    onTimeChange = (time)=>{
        this.setState({
            time
        })
    }
    render(){
        const images = [{
            imgSrc: Fig1,
            des: "Veenapani Chawla established the theatre company, Adishakti, in Mumbai, in 1981",
            
        }, {
            imgSrc: Fig2,
            des: "Koothu Kovil, Adishakti campus (Courtesy Shrinjita Biswas)",
            
        }, {
            imgSrc: Fig3,
            des: "A still from Ganapati (first staged in August 2000 at Auroville).",
        }];
        return (
        <div className="blog">
            <Navbar />
            <Banner image={BannerImage} author="" description=""/>

            <Author />
            <Title title="Adishakti: An avant-garde language of theatre" />

            <br/>
            <div className="subblog">
            <Accordion allowZeroExpanded ={true}>
            <AccordionItem>
                <AccordionItemHeading>
                    <AccordionItemButton>
                        Introduction
                        </AccordionItemButton>
                    </AccordionItemHeading>
                    <AccordionItemPanel>
                    <p>
                    ‘In <Tooltip tooltip={<u>Europe</u>}>
              <strong>Europe</strong>
              <br /> <br/>
              <div id="cardContainer">
              <img style={{float:'left', padding: '10px'}} width="150px" id="demoimage" src="https://upload.wikimedia.org/wikipedia/commons/thumb/4/44/Europe_orthographic_Caucasus_Urals_boundary_%28with_borders%29.svg/220px-Europe_orthographic_Caucasus_Urals_boundary_%28with_borders%29.svg.png"/>
              <p>Europe is a continent located entirely in the Northern Hemisphere and mostly in the Eastern Hemisphere. It comprises the westernmost part of Eurasia and is bordered by the Arctic Ocean to the north, the Atlantic Ocean to the west, the Mediterranean Sea to the south, and Asia to the east.</p>
              </div>
            </Tooltip>, theatre is dying like the epidemic,’ wrote Eugenio Barba, the director of the Odin Teatret (Odin Theatre, Denmark) to the Indian theatre practitioner Veenapani Chawla (<a href="#fig1">Fig.1</a>) in 1996. Chawla shared Barba’s lament. In India, too, both contemporary and traditional forms of theatre were facing a decline in the eighties and nineties. The eighties and nineties underwent rapid cultural displacement, transition in spectatorship and side-lining of theatre as a medium for mass entertainment, partially owing to globalisation, rapid urbanisation and the coming of cable TV (in 1991). Mainstream theatre had already saturated its innovative capacities and was caught in a limbo of institutionalised theatrical rigidity.
                    </p>
                    {/*<EmbedImage id="fig1" image={Fig1} description="Veenapani Chawla (1947-2014), founder of Adishakti Theatre (Courtesy: Adishakti Archives) " />*/}
                    <ImageGallery id="fig1" image={Fig1} index={0}/>
                    <p>Since the seventies, experimental theatre in India had emerged as a counter movement to mainstream theatre and its institutionalised practices. Exploring folk forms on a regional basis became the major thrust of this new theatre. Vanguards like Kavalam Narayana Panikkar and Ratan Thiyam were looking back at ancient texts and folk practices, shaping the ‘theatre of the roots’. While, Panikkar’s theatre was grounded into the folk and classical performance traditions of Kerala, incorporating techniques of the martial art form kalaripayattu, Thiyam was re-defining his traditions through the revival of Manipuri martial art form like Thang-Ta. Veenapani Chawla (1947–2014) too came from the same line and is considered the pioneer of modern experimental theatre in India. This article explores the theatre techniques and methodology that she established through her theatre group, Adishakti.</p>
                        <p>The theatre that Veenapani Chawla moulded is unique. As playwright Mahesh Elkunchwar writes, ‘Veenapani Chawla’s theatre is interesting and intriguing not because it is avant-garde but because, instead of either deliberately eschewing tradition or exploiting ulterior motives, as is the case in most such theatres where tradition plays a pivotal role, either by its deliberate absence or by its too facile “interpretation”, it glories in tradition, peeling away its superfluities and laying bare its strengths.’ Veenapani Chawla’s quest was to create a form where the ‘live presence’ of the performer, with the aid of performance techniques, could have a sensorial impact on the spectator otherwise glued on global television.</p>
                        <p>Veenapani Chawla established Adishakti in Mumbai (Bombay) in the year 1981. Eliminating rigidities of traditional forms and restrictions of Eurocentric practices and ideas, Adishakti emerged as a space that brought together multiple forms and ideas, engaged in re-interpretation of myth and history, created a dialogue between traditional and contemporary, and served as a home for ‘multiple inputs across time, space and other cultures.’   </p>
                        <p>In the initial years, the company concentrated on performing acclaimed classics like Sophocle’s Oedipus (1981), Stoppard’s Rosencrantz and Guildenstern are Dead (1983) and Euripedes’ Trojan Women (1984). The last play of Adishakti in Mumbai, before it shifted its base to Puducherry (Pondicherry), was A Greater Dawn (1992), a play adapted from Savitri, an epic poem by Sri Aurobindo.</p>
                        <CarouselSlider slideItems = {images} />;
                        <p>In order to develop a new language of contemporary performance and evolve a multi-dimensional aesthetic, Veenapani incorporated research as part of Adishakti’s curriculum, within two years of its establishment. The traditional knowledge system contained in forms like kutiyattam, dhrupad singing, kalaripayattu, asanas and breathing exercises of yoga, vedic chanting, etc. were explored. Between 1983 and ’84, Chawla travelled to the interiors of Odisha and learnt the form of Mayurbhanj chhau and continued her study under Guru Krishna Chandra Naik in Delhi. The research on traditional forms was brought to confluence with Western techniques of performance, with which Veenapani was already familiar, as she had worked with institutions like Barba’s Odin Theatre and the Royal Shakespeare Company in the United Kingdom.</p>
                        <p>Veenapani shifted her base to Puducherry in 1993 and founded Adishakti Laboratory for Theatre Arts and Research. Situated eight kilometres away from the main town of Pondicherry, Adishakti was set up on a plot of land gifted to her by a resident of Auroville, at the outskirts of the experimental town of Auroville in 1994 (<a href="#fig2">Fig.2</a>).</p>
                        <ImageGallery id="fig2" image={Fig2} index={1}/>
                        {/*<EmbedImage id="fig2" image={Fig2} description="Koothu Kovil, Adishakti campus (Courtesy Shrinjita Biswas)" />*/}
                    </AccordionItemPanel>
            </AccordionItem>
            <AccordionItem>
                <AccordionItemHeading>
                    <AccordionItemButton>
                        The practices/ methodology of Adishakti
                        </AccordionItemButton>
                    </AccordionItemHeading>
                    <AccordionItemPanel>
                    <Accordion allowZeroExpanded ={true}>
                        <AccordionItem>
                            <AccordionItemHeading>
                                <AccordionItemButton>
                                    Physical training and methods of acting
                                </AccordionItemButton>
                            </AccordionItemHeading>
                            <AccordionItemPanel>
                                <p>‘There is a world-famous theatre director called Jerzy Grotowski and he said that there is only one modern theatre thinker and philosopher, Stanislavski. Rest of all the other theatre directors are in disagreement against Stanislavski.’
                                —Vinay Kumar, Artistic Director, Adishakti (Shrinjita Biswas in conversation with Vinay Kumar, Pondicherry, March 2018)  </p>
                                <p>Modern Indian theatre relies very heavily on psychological realism, a system of method acting developed by Russian theatre director Konstantin Stanislavski. In method acting the actor has to recall from his/her memory, the emotional experience analogous to the character he/she is playing, through the ‘art of experiencing’.</p>
                                <div style={{padding:"1px"}} >
                            <YtWrap id="O9S3tFECUOU" style={{
                                width:"100%"
                            }}  video={{
                                autoplay:1,
                                start:this.state.time
                                }}/>
                            </div>
                            <div className="banner-details">
                                <div className="left-des">
                In this video, the artistes at Adishakti speak about the methodology that they employ in their productions.
                </div>
                <div className="right-des">
                Shrinjita Biswas <br/> © Sahapedia
                </div>
            </div>
                                <p>The present artistic director of Adishakti, Vinay Kumar, while talking about its performance methodology, says, ‘What makes these traditional performers so vibrant? What are they doing in their performances? How can they perform day after day with the same vigour and intensity? These are the questions that Veenapani had turned to.’ Veenapani extracted answers from a plethora of Indian traditional knowledge systems like kutiyattam, dhrupad singing, Mayurbhanj chhau, kalaripayattu, gusti (Indian wrestling), etc., and eventually implemented an acting methodology that was so different from method acting. </p>
                                <p>According to the Natyashastra, dharmi (characteristics) are of two kinds in abhinaya (emoting)—lokadharmi and natyadharmi. While lokadharmi acting is realistic and implies enactments of daily behaviour or practices, the natyadharmi is stylised and follows the classical rules of acting as laid down in Natyashastra. </p>
                                <p>The resident-performers at Adishakti follow a fixed everyday routine. Training in kalaripayattu, swimming, asanas, gym, badminton, etc. are an integral part and the first fold of their training of the body. The performers develop a particular performance behaviour enabling them to express various emotions. The body becomes solely the medium of expression. To reflect on a contemporary sensibility and an understanding of a performative body, Adishakti engaged in a revived understanding of kalaripayattu, introducing breathing techniques and ways of balancing the body axis. Adishakti performers rely on the ancient knowledge of chakras or the centres in the body, as defined in the Tantric tradition. The individual chakra is a centre for body dynamics, when correctly used facilitates body movements: the swadhishtan chakra (below the abdomen facilitates body balance), the mooladhara (lowest chakra at the base of the spine) and the manipura chakra (at the navel) articulates half-sitting postures with knees pointed outwards. Being aware of the chakras, the performer is able to articulate movements from one centre to the other by shifting weight and balance. Other than activating the body movements and vigour, understanding of the chakras helps the performer in psychological expressivity and impacts the voice and its expression. </p>
                            </AccordionItemPanel>
                        </AccordionItem>
                        <AccordionItem>
                            <AccordionItemHeading>
                                <AccordionItemButton>
                                Breath: The medium of expression
                                </AccordionItemButton>
                            </AccordionItemHeading>
                            <AccordionItemPanel>
                            <p>Natyashashtra describes nine basic expressions of emotions stimulating rasas, which are used in both lokadharmi and natyadharmi: sringara (erotic), vira (heroic), raudra (terror), bibhatsa (disgust), hasya (humour), karuna (pathos, compassion), adbhuta (wonder or shock), bhayanaka (dread) and shantha (peace or tranquillity) deriving from their respective bhavas (emotions). The ultimate expressivity of these emotions is stimulated through contact with external situations called vibhavas, leading to its reactions (usually physical) called anubhavas. When an actor’s performance makes the spectator feel his condition, this audience reaction towards the actor’s condition is called rasa. </p>
                            <p>To realise the bhavas, Adishakti uses breath as the medium, again contrary to the Stanislavskian methodology of acting. Adishakti modified on the codified breath patterns of kutiyattam, corresponding to the nine emotions described in the Natyashastra. Articulation of each breath pattern generates individual emotion by stimulating the chakra of origin of that particular emotion and all three expressions—psychological, physical and vocal—are united by a common breath. Taking from the principles of mukha (facial) abhinaya in kutiyattam, the performer at Adishakti uses the same breath patterns to stimulate vocal and bodily expressions.</p>
                            <p>Veenapani Chawla explains this as follows: ‘The same breath used in expressing karuna or sorrow in Koodiyattam for mukha abhinaya, was used by Adishakti actor Vinay Kumar to express anguish in his voice and body in Brhannala (1998) when he cries as Arjuna at the death of Abhmanyu. In Koodiyattam, karuna is expressed through the face by a process in which the breath energy is pulled up from the base of the spine and stored in the chest region, while the breath energy from the neck is compressed down on this concentration in the chest. The concentration of these two energies coming from physically different parts of the body, results in the feeling of a load in the heart, creating an expression of extreme pain on the face and a contortion of the body, because it is struggling in the absence of breath. The expression of pain feeds the emotional centre and arouses real emotional pain.’</p>
                            <p>Thus, breath, the physical expression of life, forms the core essential of a performance in Adishakti. The actors are trained in breathing techniques and are expected to scrutinise their breath patterns in their daily activities. </p>
                            <ImageGallery id="fig3" image={Fig3} index={2}/>
                            </AccordionItemPanel>
                        </AccordionItem>
                        <AccordionItem>
                            <AccordionItemHeading>
                                <AccordionItemButton>
                                    Vocal expressions
                                </AccordionItemButton>
                            </AccordionItemHeading>
                            <AccordionItemPanel>
                            <p>Vocal expression assumes the second most significant element, after breath, for a performer at Adishakti. Both breath and voice form the core of emotional expressions in a performance. The voice being more reflective of the performer’s psychological condition, at Adishakti performers are made to understand vocal issues under both physical and psychological realms. </p>
                            <p>The vocal exercises are preceded by daily exercises, in order to flex the body and concentrate on breath patterns only, eliminating mental and emotional pre-occupations. In order to take control and use the organs appropriately (most importantly, the diaphragm and the lungs) related to voice production, traditional asanas like bhastrika, kapalbhati pranayamas, ardhakurmasana and other yogic breathing exercises are implemented in the performer’s curriculum. Swimming and games play an essential role in increasing the longevity of breathing capacity in the performer. To activate the vocal chords and other physical resonators or centres through breathing, Adishakti again borrows from kalaripayattu’s postures and stances, while simultaneously singing or chanting and creating a rhythm. For the development of vocal expression and also speech styles, Adishakti draws from various traditions—chanting from Sama Veda and Yajur Veda, Koranic chant, kutiyattam vachika (vocals), Buddhist and Gregorian chants—creating a new vocabulary for voice and speech in theatre altogether. </p>
                            <p>Rhythm in kutiyattam underlines the performer’s craft as an ‘unwritten or unspoken text’. Likewise, Adishakti has developed its performance texts based on rhythm. Just like the breath, rhythm is internalised in a performer at Adishakti. Other than experiencing rhythm through actions in daily life, every performer has to learn to play at least one percussion instrument, mainly the mizhavu. While playing the mizhavu, both the palms are used, right and left, activating both sides of the brain for a sharper perception of the environment and better awareness of one’s body and movements. With exploration of rhythm and music, the mizhavu rhythm is in full-swing in the play Ganapati (2000) (Fig.3), where words cease and rhythm becomes the text. </p>
                            <p>he performers have explored their craft with several musical instruments like the tabla, saxophone, drums, gongs, xolophone, rhythm-box, chenda (a kind of drum), chimes, guitar and bells. With every production, Adishakti has introduced more musical instruments on stage, a live orchestration, pivotal to the performance text. Musicians, both visiting and belonging to the core team of Adishakti, have been playing the mizhavu as well, alongside the instruments they specialise in playing—Suresh Kaliyath (tabla player and rhythm box, amongst other instruments, The Hare and the Tortoise 2007, Ganapati), Pascal Siegar (saxophonist, The Hare and the Tortoise) and Arjun Shankar (guitarist, The Hare and the Tortoise) (<a href="#fig4">Fig.4</a>). In its latest production, Bali (2018, directed by Nimmy Raphel), the onstage production of music has been replaced by a new-age music score, composed by Vinay Kumar. Popular music, folk rhythm, speech and composition on string-instruments have been configured digitally with an electronic score using the ‘drop the bass’ characterisation. This change has been deployed owing to the difficulties in accommodating a live band of musicians in the live performance. Nonetheless, the musical framework in Adishakti performances is the result of hybridisation, a core principle of its dramaturgy. </p>
                            {/*<EmbedImage id="fig4" image={Fig4} description=" Hamlet in conversation with the musicians in the play Hare and the Tortoise. (Courtesy: Adishakti Archives) " />*/}
                                <ImageGallery id="fig4" image={Fig4} index={3}/>
                            </AccordionItemPanel>
                        </AccordionItem>
                        <AccordionItem>
                            <AccordionItemHeading>
                                <AccordionItemButton>
                                    Text and Language
                                </AccordionItemButton>
                            </AccordionItemHeading>
                            <AccordionItemPanel>
                            <p>Veenapani Chawla explains a performer’s physical connection to the text for the performance by citing an example from Patsy Rodenburg’s Speaking Shakespeare, where the author quotes Paulina from The Winter’s Tale: ‘What studied torments, tyrant, hast for me?’ </p>
                            <p>The rhythm of a whole sentence comes out of a sensorial experience of the sound produced during its utterance. Adishakti explores the relation between sound and the body—how the utterance of each vowel, word and a sentence effects the movement of the mouth. Working with Chhanda Shastra (the study of Sanskrit poetic metres), Adishakti performers perceive each word in the text through a physical experience, not constraining to hearing only, but to physically feeling the text—experiencing the movement of breath while speaking, understanding the specific point of articulation in the mouth while uttering a vowel or a consonant—to create an emotional understanding of the text, rather than a critical one. </p>
                            <p>Though English is used predominantly as the language for communication in Adishakti, it follows the native speaker’s style, with ‘eclectic mixture of other Indian languages and speech rhythms’. Other than English, the actors opt for occasional use of regional languages like Marathi, Tamil, amongst others, and foreign languages like French. </p>
                            <p>The use of texts in Adishakti plays has been unconventional. Since 1992, Adishakti turned towards classical Indian texts with Shri Aurobindo’s Savitri. The journey of exploring the two South Asian epics, Ramayana and Mahabharata, started with ‘Impressions of Bhima’ staged in 1994. Adishakti’s main intention was to understand and deconstruct the epics, developing an alternative narrative and instigating parallel discourses of the same. Instead of following the mainstream narrative of the popular figures in the two epics, Adishakti sets marginalised characters as the entry point to an alternative discourse and explored the other characters through a radical perspective. Through Bhima, the second eldest amongst the Pandavas, the Adishakti text for Impressions of Bhima explores the protagonist’s declining relationships with other characters and understands how he was wronged. The production Brhannala (1998) portrays the two liminal moments in Arjuna’s life where crisis led the way to choices for the protagonist. In Bali (2018), the death of Bali raises questions of justice and puts under scrutiny the characters conspiring towards Bali’s death: his younger brother, Sugriva, and Rama. Influenced by the principles of Sri Aurobindo, Chawla found repose in engaging, intervening, reworking and editing Indian epics. For her, a myth, the finest expression of theatre, yields the possibilities of multiple interpretations and becomes ‘a secret portal, which allows the knowledge in the cosmos to pour into (our) expressions. It is a myth precisely because it is something that can resonate forever and ever. It includes within it not only things of the past, of when it was created, but also of future times, and there is that in it, a wisdom, which allows for contemporary intervention.’ </p>
                        </AccordionItemPanel>
                        </AccordionItem>
                    </Accordion>
                    </AccordionItemPanel>
            </AccordionItem>
            <AccordionItem>
                <AccordionItemHeading>
                <AccordionItemButton>
                    Conclusion
                </AccordionItemButton>
                </AccordionItemHeading>
                <AccordionItemPanel>
                <p>Adishakti’s focus has always been to engage with traditional forms, and to create a contemporary discourse in both theatre and history. Its plural aesthetics have been developed by employing many modes of expression and in totality reflects on one formal metaphor, that of human existence. Using myth as metaphor, Adishakti questions the rigidity of popular history, rejecting the latter’s practice of marginalisation and hero-worship. In building up a hybrid form of theatrical expression, its holistic mode of expression through sound, image, word and gestures makes it contemporary in approach and practice, while the narrative texts pave the way towards re-interpreting and questioning of traditional history, raising questions about caste, gender, class and functioning of the structures of the nation-state. An avant-garde language of theatre, Adishakti has broken away from the limitations of Indianness and has created a global language for theatre from India. </p>
                </AccordionItemPanel>
                </AccordionItem>
                <AccordionItem>
                <AccordionItemHeading>
                <AccordionItemButton>
                    References
                </AccordionItemButton>
                </AccordionItemHeading>
                <AccordionItemPanel>
                <ul>
                    <li>Gokhale, The Theatre of Veenapani Chawla, 98</li>
                    <li>Theatre of Roots Movement was the first conscious effort to search for the roots and form an identity of ‘Indianness through urban theatre’, where the practitioners rejected the colonial method of dramaturgy, ‘a decolonizing gesture’, in order to critique the socio-political and economic issues of the nation, with the use of traditional folk forms. According to Dharwadker, ‘the post-colonial theatre is not a seamless extension of either colonial or pre-colonial traditions, but a product of  new theoretical, textual, material, institutional and condition created by the experience of political independence, cultural autonomy and new nationhood.’</li>
                    <li>Gokhale, The Theatre of Veenapani Chawla, xiii</li>
                </ul>
                </AccordionItemPanel>
                </AccordionItem>
            </Accordion>

            </div>

            <Tags tags={['VALPUTATE ENGRISH','BAPPJIS RENTIS', 'EUISMOD DINNER', 'TIGER MAN DREAM']} />
            <Bibliography bib="Dharwadker, A.B. Theatres of Independence: Drama, Theory and Urban Performance in India since 1947. Oxford University Press, 2008.
Gokhale, S, ed. The Theatre of Veenapani Chawla: Theory, Practice, Performance. New Delhi: Oxford University Press, 2014.
Rangacharya, A. The Natyashashtra: English Translation with Critical Notes. New Delhi: Munshiram Manoharlal, 2014." 
/>
{/* <CarouselSlider slideItems = {images} />; */}
            <Footer/>
        </div>
    )
}
}
export default Blog;