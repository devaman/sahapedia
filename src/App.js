import React, { Component } from 'react';
import './App.css';
import Image1 from './assets/s1.jpg'
import Image2 from './assets/s2.jpg'
import Image3 from './assets/s3.jpg'
import Image4 from './assets/s4.jpg'
import Image7 from './assets/s7.jpg'
import Image8 from './assets/s8.jpg'
import Tiles from './components/Tiles/Tiles';
import { Switch, Route,withRouter } from 'react-router';
import Blog from './containers/Blog/Blog';
import Blog2 from './containers/Blog2/Blog2';
import Media from './containers/Media/Media';
import Search from './components/Search/Search';
const data = [
  {
    heading: "Adishakti: An avant-garde language of theatre",
    image: Image1,
    headColor: "white",

    description: "Nothing to show"
  }, {
    heading: "Descendants of Prajapati: The Kumhar Community of Potters",
    image: Image2,
    headColor: "white",
    link:"/media",
    description: "Nothing to show"

  },
  {
    heading: "In Search of White Gold: Salt Harvesting at Marakkanam",
    image: Image3,
    headColor: "white",

    description: "Nothing to show"
  }, {
    heading: "Eternal Witness of Kashmir Valley: Tracing the Jhelum River ",
    image: Image4,
    description: "Nothing to show",
    headColor: "white"

  }, {
    heading: "The Art of Aromas: Ittar Industry of Kannauj",
    image: Image7,
    description: "Nothing to show",
    headColor: "white",

  },
  {
    heading: "Taleems of Kolhapur: A Legacy in Wrestling",
    image: Image8,
    headColor: "white",

    description: "Nothing to show"
  }
]
class App extends Component {
  componentDidMount() {
    // if (!this.props.user) {
    //   this.props.history.push('/signin')
    // }
  }
  render() {
    console.log(this.props);
    
    return (
      <div className="App">
        <Switch>
          <Route path='/blog' component={Blog}/>
          <Route path='/blog2' component={Blog2}/>
          <Route path='/media' component={Media}/>
          <Route path='/search' component={Search}/>
          <Route path='/' component={Blog}/>
        </Switch>

      </div>
    );
  }
}
export default withRouter(App)