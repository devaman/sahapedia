import React from 'react';
import './Heading2.css';
const Heading2 = (props)=>{
    return(
        <h1 className="heading2">
            {props.children}
        </h1>
    )
}
export default Heading2;