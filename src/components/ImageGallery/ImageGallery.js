import React, { Component } from 'react';
import Lightbox from "react-image-lightbox";
import "react-image-lightbox/style.css";
import Image_1 from '../../assets/fig1.jpeg';
import Image_2 from '../../assets/fig2.jpeg';
import Image_3 from '../../assets/adi_3.jpeg';
import Image_4 from '../../assets/banner.jpeg';
import Image_5 from '../../assets/Image_5.jpg';
import Image_6 from '../../assets/Image_6.jpg';
import Image_7 from '../../assets/Image_7.jpg';
import Image_8 from '../../assets/Image_8.jpg';
import Image_9 from '../../assets/Image_9.jpg';
import { Link, withRouter } from 'react-router-dom';
import EmbedImage from '../../components/EmbedImage/EmbedImage';
import './ImageGallery.css'

const images = [{
    photo: Image_1,
    caption: "Veenapani Chawla established the theatre company, Adishakti, in Mumbai, in 1981",
    subcaption: "In 1993 she shifted base to Puducherry where, the next year, she finally established a permanent campus. Through her productions, Chawla constantly strove to develop a new language of contemporary performance and evolve amulti-dimensional aesthetic. Courtesy: Adishakti Theatre Archives.",
    thumbnail: Image_1,
}, {
    photo: Image_2,
    caption: "Koothu Kovil, Adishakti campus (Courtesy Shrinjita Biswas)",
    subcaption: "The play draws from an episode in Mahabharata, where an exiled Arjuna spends a year disguised as a woman, Brhannala, and learns as well as teaches how to sing and dance. Courtesy: Adishakti Theatre Archives.",
    thumbnail: Image_2,
}, {
    photo: Image_3,
    caption: "A still from Ganapati (first staged in August 2000 at Auroville).",
    subcaption: "This play is an interpretation of the cycle of birth, life, celebration and destruction, narrated through the birth story of the god Ganapati. Actors Vijay Kumar, playing themizhavu, a traditional percussion instrument from Kerala, and Pascal Sieger,playing the saxophone. Courtesy: Adishakti Theatre Archives",
    thumbnail: Image_3,
}, {
    photo: Image_4,
    caption: "Hamlet in conversation with the musicians in the play 'Hare and the Tortoise",
    subcaption: "A mizhavu performer plays withboth palms, activating both sides of the brain for a sharper perception of theenvironment and a better awareness of one’s body and movements. InGanapati, words cease and rhythm becomes the text. Courtesy: AdishaktiTheatre Archives.",
    thumbnail: Image_4,
}, {
    photo: Image_5,
    caption: "A disgruntled Arjuna. Nimmy Raphel in The Hare and the Tortoise, first staged in 2007.",
    subcaption: "The narrative runs through the unfolding of the characters and notions of Hamlet, Zeno’s Paradox, Fugue, Eklavya, Arjuna, Ganapati, Kartik, Alice, theRed Queen, the Hare and the Tortoise. As Veenapani Chawla says, Adishakti’s The Hare and the Tortoise is a dramatic meditation on the ethical possibilities inherent in the notion of contemporaneity. Courtesy: Adishakti Theatre Archives.",
    thumbnail: Image_5,
}, {
    photo: Image_6,
    caption: "Arjuna in a playful moment with Hamlet from The Hare and the Tortoise.",
    subcaption: "Nimmy Raphel as Arjuna and Vinay Kumar as Hamlet. Courtesy: Adishakt Theatre Archives",
    thumbnail: Image_6,
}, {
    photo: Image_7,
    caption: "A still from Bali, a 2018 production, directed by Nimmy Raphel.",
    subcaption: "Vinay Kumar as Bali and Rijul Ray as Sugriva. The narrative alternatively perceives the episode of killing Bali from behind, an episode from the Ramayana. Courtesy: Adishakti Theatre Archives",
    thumbnail: Image_7,
}, {
    photo: Image_8,
    caption: "A still from The Hare and the Tortoise.",
    subcaption: "Arvind Wishwanath Rane (in the front) and Arjun Shankar. Courtesy: Adishakti Theatre Archives",
    thumbnail: Image_8,
}, {
    photo: Image_9,
    caption: "A still from Bali (2018). Vinay Kumar as Bali and Rijul Ray as Sugriva.",
    subcaption: "Bali employs a new-age music score, composed by Vinay Kumar. Popular music, folk rhythms, speech and compositions on string instruments have been configured digitally with an electronic score. Courtesy: Adishakti Theatre Archives.",
    thumbnail: Image_9,
}];
class ImageGallery extends Component {
    constructor(props) {
        super(props);

        this.state = {
            photoIndex: 0,
            isOpen: false
        };
    }

    render() {
        const { photoIndex, isOpen } = this.state;
        return (
            <div className='center1'>
                 {/*<img src={images[1]['photo']} onClick={() => this.setState({ isOpen: true })} width="1100px" height="700px" className="center1"></img>*/}
                 <a onClick={() => this.setState({ isOpen: true , photoIndex: this.props.index})}>
                <EmbedImage id={this.props.id} image={this.props.image} onClick={() => this.setState({ isOpen: true })} />
                 </a>
                {isOpen && (
                    <Lightbox
                        mainSrc={images[photoIndex]['photo']}
                        mainSrcThumbnail={images[photoIndex]['photo']}
                        nextSrc={images[(photoIndex + 1) % images.length]['photo']}
                        nextSrcThumbnail={images[(photoIndex + 1) % images.length]['photo']}
                        prevSrc={images[(photoIndex + images.length - 1) % images.length]['photo']}
                        prevSrcThumbnail={images[(photoIndex + images.length - 1) % images.length]['photo']}
                        imageTitle={photoIndex+1 + "/" + images.length + ": " + images[photoIndex]['caption']}
                        imageCaption={images[(photoIndex)]['subcaption']}
                        enableZoom={false}
                        onCloseRequest={() => this.setState({ photoIndex: 0, isOpen: false })}
                        onMovePrevRequest={() =>
                            this.setState({
                                photoIndex: (photoIndex + images.length - 1) % images.length
                            })
                        }
                        onMoveNextRequest={() =>
                            this.setState({
                                photoIndex: (photoIndex + 1) % images.length
                            })

                        }
                    />
                )}

            </div>

        )
    }
}

export default withRouter(ImageGallery);

