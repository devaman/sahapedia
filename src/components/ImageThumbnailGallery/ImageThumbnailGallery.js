import React, { Component } from 'react';

import ReactBnbGallery from 'react-bnb-gallery';
import 'react-bnb-gallery/dist/style.css';
import Image_1 from '../../assets/images/Image_1.jpg';
import Image_2 from '../../assets/images/Image_2.jpg';
import Image_3 from '../../assets/images/Image_3.jpg';
import Image_4 from '../../assets/images/Image_4.jpg';
import Image_5 from '../../assets/images/Image_5.JPG';
import Image_6 from '../../assets/images/Image_6.jpg';
import Image_7 from '../../assets/images/Image_7.jpg';
import Image_8 from '../../assets/images/Image_8.JPG';
import Image_9 from '../../assets/images/Image_9.jpg';
import Image_10 from '../../assets/images/Image_10.jpg';
import Image_11 from '../../assets/images/Image-11.jpg';


const photos = [{
  photo: Image_1,
  caption: "Veenapani Chawla established the theatre company, Adishakti, in Mumbai, in 1981",
  subcaption: "In 1993 she shifted base to Puducherry where, the next year, she finally established a permanent campus. Through her productions, Chawla constantly strove to develop a new language of contemporary performance and evolve amulti-dimensional aesthetic. Courtesy: Adishakti Theatre Archives.",
  thumbnail: Image_1,
}, {
  photo: Image_2,
  caption: "Arjuna at the death of Abhimanyu. Vinay Kumar in Brhannala, first staged in 1998.",
  subcaption: "The play draws from an episode in Mahabharata, where an exiled Arjuna spends a year disguised as a woman, Brhannala, and learns as well as teaches how to sing and dance. Courtesy: Adishakti Theatre Archives.",
  thumbnail: Image_2,
}, {
  photo: Image_3,
  caption: "A still from Ganapati (first staged in August 2000 at Auroville).",
  subcaption: "This play is an interpretation of the cycle of birth, life, celebration and destruction, narrated through the birth story of the god Ganapati. Actors Vijay Kumar, playing themizhavu, a traditional percussion instrument from Kerala, and Pascal Sieger,playing the saxophone. Courtesy: Adishakti Theatre Archives",
  thumbnail: Image_3,
}, {
  photo: Image_4,
  caption: "A still from Ganapati. Actors playing mizhavu.",
  subcaption: "A mizhavu performer plays withboth palms, activating both sides of the brain for a sharper perception of theenvironment and a better awareness of one’s body and movements. InGanapati, words cease and rhythm becomes the text. Courtesy: AdishaktiTheatre Archives.",
  thumbnail: Image_4,
}, {
  photo: Image_5,
  caption: "A disgruntled Arjuna. Nimmy Raphel in The Hare and the Tortoise, first staged in 2007.",
  subcaption: "The narrative runs through the unfolding of the characters and notions of Hamlet, Zeno’s Paradox, Fugue, Eklavya, Arjuna, Ganapati, Kartik, Alice, theRed Queen, the Hare and the Tortoise. As Veenapani Chawla says, Adishakti’s The Hare and the Tortoise is a dramatic meditation on the ethical possibilities inherent in the notion of contemporaneity. Courtesy: Adishakti Theatre Archives.",
  thumbnail: Image_5,
}, {
  photo: Image_6,
  caption: "Arjuna in a playful moment with Hamlet from The Hare and the Tortoise.",
  subcaption: "Nimmy Raphel as Arjuna and Vinay Kumar as Hamlet. Courtesy: Adishakt Theatre Archives",
  thumbnail: Image_6,
}, {
  photo: Image_7,
  caption: "A still from Bali, a 2018 production, directed by Nimmy Raphel.",
  subcaption: "Vinay Kumar as Bali and Rijul Ray as Sugriva. The narrative alternatively perceives the episode of killing Bali from behind, an episode from the Ramayana. Courtesy: Adishakti Theatre Archives",
  thumbnail: Image_7,
}, {
  photo: Image_8,
  caption: "A still from The Hare and the Tortoise.",
  subcaption: "Arvind Wishwanath Rane (in the front) and Arjun Shankar. Courtesy: Adishakti Theatre Archives",
  thumbnail: Image_8,
}, {
  photo: Image_9,
  caption: "A still from Bali (2018). Vinay Kumar as Bali and Rijul Ray as Sugriva.",
  subcaption: "Bali employs a new-age music score, composed by Vinay Kumar. Popular music, folk rhythms, speech and compositions on string instruments have been configured digitally with an electronic score. Courtesy: Adishakti Theatre Archives.",
  thumbnail: Image_9,
}, {
  photo:Image_10,
  caption: "A still from Bali (2018).",
  subcaption: "Kiyomi Mehta (left) and Ashiqa Salvan as Kili and Moli. Courtesy: Adishakti Theatre Archives",
  thumbnail: Image_10,
}, {
  photo: Image_11,
  caption: "Vinay Kumar in Bali.",
  subcaption: "In her productions, Veenapani Chawla drew inspiration from the art forms of kutiyattam, dhrupad singing, Mayurbhanj chhau, kalaripayattu, gusti, etc. and eventually implemented an acting methodology that was very different from method acting. Courtesy: Adishakti Theatre Archives",
  thumbnail: Image_11,
}];

class ImageThumbnailGallery extends Component {
  constructor() {
    super(...arguments);
    this.state = { galleryOpened: false };
    this.toggleGallery = this.toggleGallery.bind(this);
  }

  toggleGallery() {
    this.setState(prevState => ({
      galleryOpened: !prevState.galleryOpened
    }));
  }

  render () {
    return (
        <div className='row'>
            {/* <button >Open photo gallery</button> */}
            {/* <div class="col-md-3 card card-image"
  style={{backgroundImage: 'url(' + Image_3 + ')'}}>

  
        <div class="text-white text-center d-flex align-items-center rgba-black-strong py-5 px-4">
          <div>
            <h5 class="pink-text"><i class="fas fa-chart-pie"></i> Adishakti</h5> */}
            {/* <h3 class="card-title pt-2"><strong>Gallery</strong></h3> */}
            {/* <p>Shrinjita Biswas is a writer and a researcher based in New Delhi.
               She has been engaged as a script writer and experiments with the mediums of sound and photography.</p> */}
            <a class="btn btn-green" onClick={this.toggleGallery}><i class="fas fa-image left"></i> View Image gallery</a>
          {/* </div>
        </div>

      </div> */}
            <ReactBnbGallery
                show={this.state.galleryOpened}
                photos={photos}
                onClose={this.toggleGallery} />
                
        </div>
    )
  }
}

export default ImageThumbnailGallery;