import React from 'react';
import './SectionMap.css'
const SectionMap = (props)=>{
    return(
        <div className="section-map">
            <ul className="map">
                <li style={{width:"20%",backgroundColor:"orange"}}><span>1</span></li>
                <li style={{width:"20%",backgroundColor:"rgb(210,224,231)"}}><span></span></li>
                <li style={{width:"10%",backgroundColor:"rgb(188,200,206)"}}><span>2</span></li>
                <li style={{width:"40%",backgroundColor:"rgb(167,179,185)"}}><span>3</span></li>
                <li style={{width:"10%",backgroundColor:"rgb(138,150,156)"}}><span>4</span><span className="end">SECTION MAP</span></li>
                
            </ul>
        </div>
    )
}
export default SectionMap;