import React from 'react';
import './Tags.css';
const Tags = (props)=>{
    let tags = props.tags.map((d,i)=>{
        return <span key={i}>{d}</span>
    })
    return(
        <div className="tags">
            {tags}
        </div>
    )
}
export default Tags;