import React from 'react';
import Drift from 'drift-zoom';
// import {magnify} from '../ZoomLens/ZoomLens'
// import Magnifier from 'react-magnifier';

class ZoomOnHover extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            paneShown: false
        }
    }

    componentDidMount(){
        // Trying with the magnify function
        // console.log("Using magnify function ");
        // magnify(".carousel-cell-image", 3);

        console.log("Creating the lens here..");
        var glass = document.createElement("DIV");
        glass.setAttribute("className", "img-zoom-lens");
        
        // var img = document.querySelector(".embed-image");
        // img.parentElement.insertBefore(glass, img);

        console.log("Glass: " + glass.getAttribute("className"));
        var demoTrigger = document.querySelector("img");
        demoTrigger.parentElement.insertBefore(glass, demoTrigger);
        var inlineContainer = document.querySelector(".left-des");
        console.log(demoTrigger);
        console.log(inlineContainer);

        new Drift(demoTrigger, {
          zoomFactor: 3,
          showWhitespaceAtEdges: true,
          inlineOffsetX: 0,
          inlineOffsetY: 0,
          inlinePane: true,
          inlineContainer: inlineContainer,
          sourceAttribute: 'src',
          hoverBoundingBox: true,
        });
    
        console.log("Drift object created");  
        // console.log("src: " + img.getAttribute('src'));
        // return <Magnifier src={img.getAttribute('src')} width={500}/>;  
    }

    render(){
        return (
            <> </>
        );
    }
}

export default ZoomOnHover;