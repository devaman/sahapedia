import React from 'react';
import './FullSizeImage.css';
const FullSizeImage = (props)=>{
    return(
        <div className="full-size-image">
            <img src={props.image}/>
            <div className="banner-details">
                <div className="left-des">
               {props.description}
                </div>
                <div className="right-des">
                FERMENTUM MAGNA 1986 <br/> © AMET RISUS
                </div>
            </div>
        </div>
    )
}
export default FullSizeImage;