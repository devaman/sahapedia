import React from 'react';
import './Slidable.css';

class Slidable extends React.Component {
    constructor(props) {
      super(props);
    }
  
    render() {
        const caption = this.props.caption;
        const imageSrc = this.props.image;
        console.log("Caption: " + caption);
        console.log("imageSrc: " + imageSrc)
        return (
            <div className="CarouselSlide">
                <img src={imageSrc} alt="Image here" class="CarouselImage" />
                <div className="CarouselCaption">{caption}</div>
                {console.log(caption)}
            </div>
        );
    }
}
export default Slidable;