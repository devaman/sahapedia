import React from 'react';
import Tile from './Tile/Tile';
import "./Tiles.css"
const Tiles = (props)=>{
    
    let data =  props.data.map((d,i)=>{
        return <Tile history={props.history} key={i} {...d} />

    })
    return <div className="tiles" style={{width:props.type==2?"100%":""}}>
        {data}
    </div>
}
export default Tiles;