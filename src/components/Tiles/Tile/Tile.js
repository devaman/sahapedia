import React from 'react';
import { usePalette } from 'react-palette'
import './Tile.css'
const Tile = (props) => {
    const { data, loading, error } = usePalette(props.image)
    return (
        <div className="tile" style={props.style} onClick={()=>{props.link?props.history.push(props.link):props.history.push('/blog')}}>
            <img style={{ boxShadow: `0px 1px 3px ${data.darkMuted}` }} src={props.image} />
            <div className="text" style={props.styleText}>
                <h1 style={{color:props.headColor?props.headColor:data.lightVibrant}}>{props.heading}</h1>
                <p style={{color:"#fff"}}>{props.description}
                </p>
            </div>
        </div>
    )
}
export default Tile;