import React, { useState } from 'react';
import Flickity from 'react-flickity-component';
import S1 from '../../assets/Image_1.jpg';
import S2 from '../../assets/Image_2.jpg';
import S3 from '../../assets/Image_3.jpg';
import S4 from '../../assets/Image_4.jpg';
import S5 from '../../assets/Image_5.jpg';
import S6 from '../../assets/Image_6.jpg';
import S7 from '../../assets/Image_7.jpg';
import S8 from '../../assets/Image_8.jpg';
import S9 from '../../assets/Image_9.jpg';
import Gallery from 'react-grid-gallery';


const IMAGES =
[
{
        src: S2,
        thumbnail: S2,
        thumbnailWidth: 308,
        thumbnailHeight: 200,
        caption: "Veenapani Chawla established the theatre company Adishakti in Mumbai in 1981. In 1993, she shifted base to Puducherry where, the next year, she finally established a permanent campus. Through her productions, Chawla constantly strove to develop a new language of contemporary performance and evolve a multi-dimensional aesthetic."
},

{
        src: S3,
        thumbnail: S3,
        thumbnailWidth: 308,
        thumbnailHeight: 200,
        caption: "A still from 'Ganapati' (first staged in August 2000 at Auroville). This play is an interpretation of the cycle of birth, life, celebration and destruction, narrated through the birth story of the god Ganapati. Actors Vinay Kumar, playing the mizhavu, a traditional percussion instrument from Kerala, and Pascal Sieger, playing the saxophone."
},
{
        src: S4,
        thumbnail: S4,
        thumbnailWidth: 308,
        thumbnailHeight: 200,
        caption: "A still from 'Ganapati'. Actors playing mizhavu. A mizhavu performer plays with both palms, activating both sides of the brain for a sharper perception of the environment and a better awareness of one’s body and movements. In 'Ganapati', words cease and rhythm becomes the text."
},
{
        src: S5,
        thumbnail: S5,
        thumbnailWidth: 275,
        thumbnailHeight: 200,
        caption: "A disgruntled Arjuna. Nimmy Raphel in 'The Hare and the Tortoise', first staged in 2007. The narrative runs through the unfolding of the characters and notions of Hamlet, Zeno’s Paradox, Fugue, Eklavya, Arjuna, Ganapati, Kartik, Alice, the Red Queen, the hare and the tortoise. As Veenapani Chawla says, Adishakti’s 'The Hare and the Tortoise' is a dramatic meditation on the ethical possibilities inherent in the notion of contemporaneity."
},

{
        src: S6,
        thumbnail: S6,
        thumbnailWidth: 308,
        thumbnailHeight: 200,
        caption:"Arjuna in a playful moment with Hamlet in 'The Hare and the Tortoise'. Nimmy Raphel as Arjuna and Vinay Kumar as Hamlet."
},
{
        src: S7,
        thumbnail: S7,
        thumbnailWidth: 308,
        thumbnailHeight: 200,
        caption:"A still from 'Bali', a 2018 production, directed by Nimmy Raphel. Vinay Kumar as Bali and Rijul Ray as Sugriva. The narrative alternatively perceives the episode of killing Bali from behind, an episode from the Ramayana."
},


]

const GalleryGrid = (props)=>{

    
    return (
        <Gallery images={IMAGES} enableImageSelection={false}/>
    );
}
export default GalleryGrid