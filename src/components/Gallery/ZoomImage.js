import React, { useState } from 'react';
import {
    Magnifier,
    GlassMagnifier,
    MOUSE_ACTIVATION,
    TOUCH_ACTIVATION
} from "react-image-magnifiers";
import S2 from '../../assets/Image_2.jpg';

const ZoomImage = (props)=>{

    
    return (
        <GlassMagnifier
            imageSrc={S2}
            imageAlt="Alttext"
        />
    );
}
export default ZoomImage;