import React from 'react';
import Flickity from 'react-flickity-component';
import S1 from '../../assets/Image_2.jpg';
import S2 from '../../assets/Image_3.jpg';
import S3 from '../../assets/Image_4.jpg';
import S4 from '../../assets/Image_5.jpg';
import S5 from '../../assets/Image_6.jpg';
import S6 from '../../assets/Image_7.jpg';
import I1 from '../../assets/Carousel/Image_1.jpg';
import I2 from '../../assets/Carousel/Image_2.jpg';
import I3 from '../../assets/Carousel/Image_3.jpg';
import I4 from '../../assets/Carousel/Image_4.jpg';
import I5 from '../../assets/Carousel/Image_5.jpg';
import I6 from '../../assets/Carousel/Image_6.jpg';
import I7 from '../../assets/Carousel/Image_7.jpg';
import I8 from '../../assets/Carousel/Image_8.jpg';
import I9 from '../../assets/Carousel/Image_9.jpg';
import I10 from '../../assets/Carousel/Image_10.JPG';
import I11 from '../../assets/Carousel/Image_11.jpg';
import I12 from '../../assets/Carousel/Image_12.jpeg';
import I13 from '../../assets/Carousel/Image_13.jpeg';
import I14 from '../../assets/Carousel/Image_14.jpeg';
import I15 from '../../assets/Carousel/Image_15.jpeg';

import 'flickity-fullscreen';
import 'flickity/dist/flickity.min.css';
import Magnifier from "react-magnifier";
import './Carousel.css';
import Slidable from '../Slidable/Slidable'
const flickityOptions = {
    fullscreen: false,
    setGallerySize: false,
    draggable: true,
    wrapAround: true
}

class Carousel extends React.Component{
    constructor(props){
        super(props);
        this.state= {
            fullscreen: false,
            setGallerySize: false,
            draggable: true,
            wrapAround: true, 
            pageDots: true
        }
    }
    // updateState = () =>{
    //     console.log("Resized..");
    //     if(window.innerWidth<910){
    //         this.setState({"pageDots":false});
    //         console.log(this.state);
    //     }
    // }
    // componentDidMount(){
    //     window.addEventListener("resize", this.updateState);
    // }
    render(){
        var imageArray = [
            {"caption": "Arjuna at the death of Abhimanyu. Vinay Kumar in 'Brhannala', first staged in 1998.", "image":I1}, 
            {"caption": "Veenapani Chawla established the theatre company Adishakti in Mumbai in 1981", "image":I2},
            {"caption": "A still from 'Ganapati' (first staged in August 2000 at Auroville)", "image":I3},
            {"caption": "A still from 'Ganapati'. Actors playing mizhavu.", "image":I4},
            {"caption": "A disgruntled Arjuna. Nimmy Raphel in 'The Hare and the Tortoise', first staged in 2007", "image":I5},
            {"caption": "Arjuna in a playful moment with Hamlet in 'The Hare and the Tortoise'.", "image":I6},
            {"caption": "A still from 'Bali', a 2018 production, directed by Nimmy Raphel.", "image":I7},
            {"caption": "A still from 'Bali' (2018). Vinay Kumar as Bali and Rijul Ray as Sugriva.", "image":I8},
            {"caption": "A still from 'Bali' (2018). Kiyomi Mehta (left) and Ashiqa Salvan as Kili and Moli", "image":I9},
            {"caption": "A still from 'The Hare and the Tortoise'. Arvind Wishwanath Rane (in the front) and Arjun Shankar", "image":I10},
            {"caption": "Vinay Kumar in 'Bali'. In her productions, Veenapani Chawla drew inspiration from various art forms", "image":I11},
            {"caption": "Hamlet in conversation with the musicians in the play 'Hare and the Tortoise'. (Courtesy: Adishakti Archives)", "image":I12},
            {"caption": "Veenapani Chawla (1947–2014), founder of Adishakti Theatre", "image":I13},
            {"caption": "Koothu kovil or performance space, Adishakti campus", "image":I14},
            {"caption": "A still from ‘Ganapati’ (Courtesy Adishakti Archives)", "image":I15}
        ];
        var elements = [];
        for(var i=0;i<imageArray.length;i++){
            elements.push(<div class="carousel-cell"><Slidable image={imageArray[i].image} caption={imageArray[i].caption} /></div>);
        }
        return(
            <Flickity
            className={'carousel'} // default ''
            elementType={'div'} // default 'div'
            options={this.state} // takes flickity options {}
            disableImagesLoaded={false} // default false
            reloadOnUpdate // default false
            
            // static // default false
            >
                {elements}
                {/* <div class="carousel-cell"><Slidable  image={S1} caption="Veenapani Chawla established the theatre company Adishakti in Mumbai in 1981"/></div>
                <div class="carousel-cell"><Slidable  image={S2} caption="Veenapani Chawla established the theatre company Adishakti in Mumbai in 1981"/></div>
                <div class="carousel-cell"><Slidable  image={S3} caption="Veenapani Chawla established the theatre company Adishakti in Mumbai in 1981"/></div>
                <div class="carousel-cell"><Slidable  image={S4} caption="Veenapani Chawla established the theatre company Adishakti in Mumbai in 1981"/></div>
                <div class="carousel-cell"><Slidable  image={S5} caption="Veenapani Chawla established the theatre company Adishakti in Mumbai in 1981"/></div>
                <div class="carousel-cell"><Slidable  image={S6} caption="Veenapani Chawla established the theatre company Adishakti in Mumbai in 1981"/></div> */}
                {/* <div class="carousel-cell"><Magnifier class="carousel-cell-image" src={S2}/><div class="caption"><p>A still from 'Ganapati' (first staged in August 2000 at Auroville). </p></div></div>
                <div class="carousel-cell"><Magnifier class="carousel-cell-image" src={S3}/><div class="caption"><p>A still from 'Ganapati'. Actors playing mizhavu. A mizhavu performer plays with both palms, activating both sides of the brain for a sharper perception of the environment and a better awareness of one’s body and movements.</p></div></div>
                <div class="carousel-cell"><Magnifier class="carousel-cell-image" src={S5}/><div class="caption"><p>Arjuna in a playful moment with Hamlet in 'The Hare and the Tortoise'. Nimmy Raphel as Arjuna and Vinay Kumar as Hamlet</p></div></div>
                <div class="carousel-cell"><Magnifier class="carousel-cell-image" src={S6}/><div class="caption"><p>A still from 'Bali', a 2018 production, directed by Nimmy Raphel. Vinay Kumar as Bali and Rijul Ray as Sugriva. The narrative alternatively perceives the episode of killing Bali from behind, an episode from the Ramayana</p></div></div> */}
                {/* <img src="../../img/Image_5_2.jpg"/>       
                <img src="../../img/Image_5_2.jpg"/>  
                <img src="../../img/Image_5_2.jpg"/>  
                <img src="../../img/Image_5_2.jpg"/>  
                <img src="../../img/Image_5_2.jpg"/>   */}
                
            </Flickity>
        )
    }
    
}
export default Carousel;