import React, { Component } from 'react';
import './Banner.css';
class Banner extends Component {
    state = {
        more: false
    }
    render() {
        let classes = ["banner-text"]
        if (this.state.more) {
            classes.push('more')
        }
        return (
            <div className="media-banner">
                <div className="image">
                    <img src={this.props.image} />
                </div>
                <h1>Theatre Space of Adishaktis</h1>
                <div className={classes.join(" ")}>
                    
                    <p>Veenapani Chawla (1947‒2014) was an acclaimed theatre personality who opened new frontiers in modern Indian theatre. After finishing an MA in History, she taught history and English in schools in Mumbai. When she took drama classes for her young students, she was so fascinated by the craft that she left everything behind and embarked on a new journey in theatre. She worked with Barba’s Odin Theatre and the Royal Shakespeare Company in the United Kingdom. Back in India, she travelled across the country to train in its traditional art forms like Mayurbhanj chau, kudiyattam, dhrupad singing, kalaripayattu, etc. Chawla established the theatre space of Adishakti in Mumbai in 1981. After staging a few successful plays, Adishakti shifted its base to Puducherry (formerly Pondicherry) in 1993, and finally a permanent space was established on the outskirts of the town in 1994. Adishakti Laboratory for Theatre Arts and Research emerged as the space where performance and research went hand in hand or even merged together to become a way of life, and where the internal processes of artistes were as crucial as their performances. Shantha Gokhale says in her book Theatre of Veenapani Chawla: Theory, Practice and Performance: ‘The day would be strictly structured around the skills that her group needed to develop and the ideas they needed to imbibe in order to become complete actors’. Adishakti artistes, under Chawla’s guidance, evolved a new theatre that is a synthesis of traditional art forms but contemporary in its concern, language and idiom. The module carries an overview on the theatre space of Adishakti, a display of images from its productions and a short video where the artistes explain the dramaturgy. </p>
                </div>
                <button onClick={() => this.setState(prevState => ({ more: !prevState.more }))}>Read {this.state.more?"Less":"More"}</button>
            </div>
        )
    }
}
export default Banner;