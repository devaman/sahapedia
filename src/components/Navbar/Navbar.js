import React from 'react';
import './Navbar.css'
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import { faGalacticRepublic } from "@fortawesome/free-brands-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { withRouter } from 'react-router';
const Navbar = (props) => {
    return (
        <nav style={{backgroundColor:props.type==2?"rgb(33, 33, 33)":""}}>
            <div className="upper-nav">
                <div className="brand" style={{color:props.type==2?"white":""}} onClick={()=>props.history.push('/')}>
                    sahapedia.org
                </div>
                <ul className="left-menu">
                    <li><a style={{color:props.type==2?"white":""}} href="/search"><FontAwesomeIcon style={{ color: "rgb(123, 123, 123)", fontSize: '15px', padding: '0px 5px' }} icon={faSearch} />Search</a></li>
                    <li><a style={{color:props.type==2?"white":""}} href="#">Sign in</a></li>
                    <li className="bars"><a style={{color:props.type==2?"white":""}} href="#" >|||</a></li>
                </ul>
            </div>
            {props.type===2?null:
            <div className="bottom-nav">
                <div className="brand">
                    <FontAwesomeIcon style={{fontSize:"30px",fontWeight:"500",marginRight:"10px"}} icon={faGalacticRepublic}/>The Chattisgarh Project
                </div>
                <ul className="left-menu">
                    <li><a href="#">VULPUTATE TELLUS</a></li>
                    <li><a href="#">IPSUM</a></li>
                    <li><a href="#">PHARETRA</a></li>
                    <li><a href="#">INCEPTOS TORTOR</a></li>
                    
                </ul>
            </div>
}
        </nav>
    )
}
export default withRouter(Navbar);