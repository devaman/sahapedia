import React from 'react';
import './EmbedImage.css';
import Magnifier from 'react-magnifier';

const EmbedImage = (props)=>{
    return(
        <div id={props.id} className="embed-image">
            <Magnifier src={props.image} zoomFactor="2"/>
            <div className="banner-details">
                <div className="left-des">
                {props.description}
                </div>
                <div className="right-des">
                {/*FERMENTUM MAGNA 1986 <br/> © AMET RISUS*/}
                </div>
            </div>
        </div>
    )
}
export default EmbedImage;