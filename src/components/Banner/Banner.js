import React from 'react';
import './Banner.css'
const Banner = (props)=>{
    return(
        <div className="banner" style={{backgroundImage:`url(${props.image}`}}>
            <div className="banner-details">
                <div className="left-des">
                Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Aenean lacinia bibendum nulla
                </div>
                <div className="right-des">
                FERMENTUM MAGNA 1986 <br/> © AMET RISUS
                </div>
            </div>
        </div>
    )
}
export default Banner;