import React, { Component } from 'react';
import './Author.css';
import ImageThumbnailGallery from '../ImageThumbnailGallery/ImageThumbnailGallery';

class Author extends Component {

    render() {
        return(
            <div className="author">
                <div >
                    <span>Shrinjita Biswas</span>
                    <span>13 HRS AGO</span>
                </div>
                <div >
                    <ImageThumbnailGallery />
                </div>
            </div>
        )
    }
}
export default Author;