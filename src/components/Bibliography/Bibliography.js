import React, { Component } from 'react';
import './Bibliography.css'
class Bibliography extends Component {
    state = {
        active: "Bibliography"
    }
    render() {

        return (
            <div className="bibliography">
                <ul className="tabs">
                    <li onClick={() => { this.setState({ active: "Bibliography" }) }} className={this.state.active === "Bibliography" ? "active" : null}>Bibliography</li>
                </ul>
                <div>
                    {this.state.active === "Bibliography" ? <div>
                    {this.props.bib}
                    </div> : <div>
                        {this.props.reference}
                        </div>}
                </div>
            </div>
        );
    }
}
export default Bibliography;