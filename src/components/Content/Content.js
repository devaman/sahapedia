import React from 'react';
import './Content.css';
const Content = (props)=>{
    return(
        <div className="content-blog">
            <div className="content-blog-body">
                {props.children}    
            </div>
            <div className="content-blog-right">
                {props.reference}
            </div>

        </div>
    )
}
export default Content;